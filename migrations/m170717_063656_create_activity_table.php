<?php

use yii\db\Migration;

/**
 * Handles the creation of table `activity`.
 */
class m170717_063656_create_activity_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('activity', [
            'id' => $this->primaryKey(),
			'title' => $this->String(),
			'categoryId' => $this->Integer(),
			'statusId' => $this->Integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('activity');
    }
}
