<?php

use yii\db\Migration;

/**
 * Handles the creation of table `status`.
 */
class m170717_063941_create_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('status', [
            'id' => $this->primaryKey(),
			'name' => $this->String(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('status');
    }
}
