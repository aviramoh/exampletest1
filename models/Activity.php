<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "activity".
 *
 * @property integer $id
 * @property string $title
 * @property integer $categoryId
 * @property integer $statusId
 */
class Activity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoryId', 'statusId'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
	
	 public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert); //must be here
		
		/*$this->categoryId+=1; //פותר את בעיית ההזנה
		$this->statusId+=1;*/
		
        if ($this->isNewRecord) 
		    $this->statusId = 2;
		
		if ($this->isAttributeChanged('categoryId') && !\Yii::$app->user->can('updateActivity')) //the category changed?
				$this->categoryId= $this->getOldAttribute('categoryId');

	    return $return;
    }
	
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'categoryId' => 'Category ID',
            'statusId' => 'Status ID',
        ];
    }
	
	public function getCategoryItem()
	{
		return $this->hasOne(Category::className(), ['id' => 'categoryId']);
	}
	
	public function getStatusItem()
	{
		return $this->hasOne(Status::className(), ['id' => 'statusId']);
	}
}
