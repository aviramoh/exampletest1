<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $name
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
	
	public static function getCategoryId()
	{
		$ids = ArrayHelper::getColumn(self::find()->all(), 'id');
		return $ids;
	}

	/*public static function getCategories()
	{
		$ids = ArrayHelper::getColumn(self::find()->all(), 'name');
		return $ids;
	}*/	
	
	/*public static function getCategoryId($id)
    {
        return self::findOne(['id'=>$id]);
    }*/
	
	public static function getCategories()
	{
		$allCategories = self::find()->all();
		$allCategoriesArray = ArrayHelper::
					map($allCategories, 'id', 'name');
		return $allCategoriesArray;						
	}
	
	public static function getCategoriesWithAllCategories()
	{
		$allCategories = self::getCategories();
		$allCategories[null] = 'All Categories';
		$allCategories = array_reverse ( $allCategories, true ); //all statuses will be first in the list
		return $allCategories;	
	}
	
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
