<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "status".
 *
 * @property integer $id
 * @property string $name
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
	
	public static function getStatusId()
	{
		$ids = ArrayHelper::getColumn(self::find()->all(), 'id');
		return $ids;
	}	
	
	public static function getStatuses()
	{
		$allStatuses = self::find()->all();
		$allStatusesArray = ArrayHelper::
					map($allStatuses, 'id', 'name');
		return $allStatusesArray;						
	}
	
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
