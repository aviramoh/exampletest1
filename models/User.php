<?php

namespace app\models;
use yii\db\ActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;


class User extends ActiveRecord implements \yii\web\IdentityInterface
{
	public $role; //for role field
		
	public static function tableName()
	{
		return 'user';
	}
	
	public function rules()
	{
		return
		[
			[['username', 'password', 'auth_key'/*, 'firstname', 'lastname'*/], 'string', 'max' => 255],
			[['username', 'password'], 'required'],
			[['username'], 'unique'],
			[['categoryId'], 'integer'],
			['role', 'safe'], //for role field
			
		];
	}


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('Not supported');
	    return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['username'=>$username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }
	
    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->isCorrectHash($password, $this->password);
    }
	
	private function isCorrectHash($plaintext, $hash)
	{
		return Yii::$app->security->validatePassword($plaintext, $hash);
	}
	
			//hash password before saving
    public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert); //must be here

        if ($this->isAttributeChanged('password')) //the password changed?
            $this->password = Yii::$app->security-> //if yes, the password will be hashed
					generatePasswordHash($this->password);

        if ($this->isNewRecord) //works only if we create a new password
		    $this->auth_key = Yii::$app->security->generateRandomString(32);

	    return $return;
    }
	
	public function afterSave($insert,$changedAttributes)
    {
        $return = parent::afterSave($insert, $changedAttributes);


		if (\Yii::$app->user->can('updateUser'))//admin permission
		{	
			$return = parent::afterSave($insert, $changedAttributes);

			$auth = Yii::$app->authManager;
			$roleName = $this->role; 
			$role = $auth->getRole($roleName);
			if (\Yii::$app->authManager->getRolesByUser($this->id) == null){
				if($role != null){
					$auth->assign($role, $this->id);
				}
			} 
			else {
				$db = \Yii::$app->db;
				$db->createCommand()->delete('auth_assignment',
					['user_id' => $this->id])->execute();
				$auth->assign($role, $this->id);
			}
		}
		       return $return;
    }
	public static function getRoles()
	{

		$rolesObjects = Yii::$app->authManager->getRoles();
		$roles = [];
		foreach($rolesObjects as $id =>$rolObj){
			$roles[$id] = $rolObj->name; 
		}
		
		return $roles; 		
	}
	
	public function getCategoryItem()
    {
        return $this->hasOne(Category::className(), ['id' => 'categoryId']);
    }

}
