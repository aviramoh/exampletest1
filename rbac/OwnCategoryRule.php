<?php
//OwnCategoryRule
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 
use app\models\User;


class OwnCategoryRule extends Rule
{
	public $name = 'OwnCategoryRule'; //must be here

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) { //is loged in
			return isset($params['activity']) ? $params['activity']->categoryId == (User::findIdentity($user)->categoryId) : false;
		}
		return false;
	}
}