<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Status;
use app\models\Category;

/* @var $this yii\web\View */
/* @var $model app\models\Activity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-form">

    <?php $form = ActiveForm::begin(); ?>
	
    <?php	if ($model->isNewRecord) { //appears only on update
			echo $form->field($model, 'title')->textInput(['maxlength' => true]);
			echo $form->field($model, 'categoryId')->dropDownList(Category::getCategories()); }?>

    <?php	if (!$model->isNewRecord && !\Yii::$app->user->can('updateActivity')) { //appears only on update	
			echo $form->field($model, 'title')->textInput(['maxlength' => true]);
			echo $form->field($model, 'statusId')->dropDownList(Status::getStatuses());
			echo $form->field($model, 'categoryId')->dropDownList(Category::getCategories(), ['disabled' => true]); } ?>
    
    <?php	if (!$model->isNewRecord && \Yii::$app->user->can('updateActivity')) { //appears only on update			
			echo $form->field($model, 'title')->textInput(['maxlength' => true]);
			echo $form->field($model, 'statusId')->dropDownList(Status::getStatuses());
			echo $form->field($model, 'categoryId')->dropDownList(Category::getCategories());}?>
			
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
