<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Category;
/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
	
    <?= $form->field($model, 'role')->dropDownList(User::getRoles(), ['disabled' => !\Yii::$app->user->can('upgradeUser'), 'prompt' => '--select role--']) ?>	

    <?/*= $form->field($model, 'auth_key')->textInput(['maxlength' => true])*/ ?>
	
	<? if (!$model->isNewRecord)
		{
			/*echo $form->field($model, 'role')->dropDownList(User::getRoles());*/
			echo $form->field($model, 'categoryId')->dropDownList(Category::getCategories());
		} ?>
			

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
